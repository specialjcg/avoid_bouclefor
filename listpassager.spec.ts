interface DownloadTiktok {
    pays: string;
    download: number;
}

const dataTikTok: DownloadTiktok[] = [{"pays": "Brazil", "download": 28},
    {"pays": "Egypt", "download": 24},
    {"pays": "Latvia", "download": 96},
    {"pays": "China", "download": 75},
    {"pays": "Ecuador", "download": 39},
    {"pays": "China", "download": 53},
    {"pays": "Czech Republic", "download": 58},
    {"pays": "Thailand", "download": 24},
    {"pays": "Ethiopia", "download": 83},
    {"pays": "Russia", "download": 83}]
/*const isEqual = (element: DownloadTiktok, searchElement: DownloadTiktok) => {
    if (element.pays !== searchElement.pays) return false
    return element.download === searchElement.download;

};
const fillFrom=(start:number,length:number):number[]=>{
    return Array(length).fill(0).map((_,index)=>index+start);
}
const positionOf = (dataTikTok: DownloadTiktok[], searchElement: DownloadTiktok[]) => {
    for(const position in dataTikTok){
        const dataTikTokPartial=dataTikTok.slice(+position);
        if (topEqual(dataTikTokPartial,searchElement)){
            return fillFrom(+position,searchElement.length);
        }
    }
    throw new Error();
};
const topEqual = (dataTikTokPartial: DownloadTiktok[],searchElement: DownloadTiktok[]) => {
    return searchElement.map((search,index)=>isEqual(dataTikTokPartial[index],search))
        .every(ispresent=>ispresent);
};*/

const isEqual = (element: DownloadTiktok, searchElement: DownloadTiktok) => {
    if (element.pays !== searchElement.pays) return false
    return element.download === searchElement.download;

};

const positionOf = (dataTikTok: DownloadTiktok[], searchElement: DownloadTiktok[]) => {
    let result: number[] = []

    for (const search of searchElement) {

        result = [...result, dataTikTok.findIndex(element => isEqual(element, search))]
        if (result.length > 1 && result[result.length - 1] - 1 !== result[result.length - 2]) throw new Error();
    }
    return result;
};


describe('test passager', () => {

    it('should find position of one element  ', () => {
        const searchElement: DownloadTiktok[] = [{"pays": "Czech Republic", "download": 58}]
        const position: number[] = positionOf(dataTikTok, searchElement);
        expect(position).toStrictEqual([6]);
    });
    it('same element should be equal', () => {
        const first: DownloadTiktok = {"pays": "Czech Republic", "download": 58}
        const second: DownloadTiktok = {"pays": "Czech Republic", "download": 58}

        expect(isEqual(first, second)).toBe(true)
    });
    it('should not be equal with different pays', () => {
        const first: DownloadTiktok = {"pays": "Czech Republic", "download": 83}
        const second: DownloadTiktok = {"pays": "Russia", "download": 83}

        expect(isEqual(first, second)).toBe(false)
    });
    it('should not be equal with different download', () => {
        const first: DownloadTiktok = {"pays": "Czech Republic", "download": 83}
        const second: DownloadTiktok = {"pays": "Czech Republic", "download": 58}

        expect(isEqual(first, second)).toBe(false)
    });
    it('should find position of two elements follewed  ', () => {
        const searchElement: DownloadTiktok[] = [{"pays": "Czech Republic", "download": 58}, {
            "pays": "Thailand",
            "download": 24
        }]
        const position: number[] = positionOf(dataTikTok, searchElement);
        expect(position).toStrictEqual([6, 7]);
    });
    it('should not find position of two elements not followed ', () => {
        const searchElement: DownloadTiktok[] = [{"pays": "Czech Republic", "download": 58}, {
            "pays": "Russia",
            "download": 83
        }]
        expect(() => positionOf(dataTikTok, searchElement)).toThrow();
    });
    it('should find position of tree elements follewed  ', () => {
        const searchElement: DownloadTiktok[] = [
            {"pays": "Czech Republic", "download": 58},
            {"pays": "Thailand","download": 24},
            {"pays": "Ethiopia", "download": 83}
            ]
        const position: number[] = positionOf(dataTikTok, searchElement);
        expect(position).toStrictEqual([6, 7, 8]);
    });
    it('should not find position of two elements not followed ', () => {
        const searchElement: DownloadTiktok[] = [
            {"pays": "Czech Republic", "download": 58},
            {"pays": "Thailand","download": 24},
            {"pays": "Russia", "download": 83}
            ]
        expect(() => positionOf(dataTikTok, searchElement)).toThrow();
    });
});
